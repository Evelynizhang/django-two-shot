from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateExpenseCategoryForm,
    CreateAccountForm,
)


@login_required
def receipt_list(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"all_receipts": all_receipts}
    return render(request, "receipts/receipt_list.html", context)


def redirect_receipts(request):
    return redirect("home")


@login_required
def view_create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    all_expense_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"all_expense_list": all_expense_list}
    return render(request, "receipts/expense_categories.html", context)


@login_required
def account_list(request):
    all_account_list = Account.objects.filter(owner=request.user)
    context = {"all_account_list": all_account_list}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
