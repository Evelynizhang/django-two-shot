from django.urls import path
from accounts.views import view_login_form, view_logout, view_signup_form


urlpatterns = [
    path("login/", view_login_form, name="login"),
    path("logout/", view_logout, name="logout"),
    path("signup/", view_signup_form, name="signup"),
]
